# Multiple Points Route

Webservice that compute a route between points. Routes can be computed using a large range of middle points. This app uses third-party web services to compute routes (ORS, HERE, MapBox ...)

API written in python using Flask

OpenApi can be found [HERE](https://api.mpr.guilhemallaman.net/openapi/swagger) (Swagger)


### Setup

- Install dependencies: `poetry install`
- Install pre-commit hooks: `poetry run pre-commit install`
- Run code-formatting tools :
```bash
poetry run pre-commit run --all-files
poetry run black [-l 128] .
poetry run isort .
poetry run mypy . --ignore-missing-import
```

- Upgrade pip dependencies :
```bash
# see
poetry-up --dry-run # checks which deps to update
poetry-up --no-commit
poetry install
```

### Serve

Flask app needs following env variables:

- optional env

```bash
# Flask env settings
export FLASK_ENV=production                 # default: "development"
export FLASK_DEBUG=True                     # default: False
# cache
export CACHE_REDIS_HOST=my.domain           # default: "localhost"
export CACHE_REDIS_PORT=12345               # default: 6379
```

- required env variables

```bash
# API version (should be fetched from poetry's pyproject.toml)
export API_VERSION=$(poetry version -s)
# ORS service
export ORS_API_KEY=a1b2c3d4...
# Docker
export APP_BIND_PORT=12345                  # port on host binded by Docker
```

Serve commands:

```bash
# using uwsgi server (WARNING: scheduler won't work correctly)
# gives more control about meta: workers, logging, sockets ...
poetry run uswgi <UWSGI-CONFIG>.ini

# standalone flask (recommended)
export FLASK_APP=app.py
poetry run flask run
```

### Build

Build docker image with `docker build -t mpr-api:latest .`

Build and run docker-compose setup with `docker-compose up --build`.
This will run flask server with uwsgi exposed on port set by `$APP_BIND_PORT` env variable

### Test

Following commands can be used to test:

```bash
# route + segments
curl -H "Content-Type: application/json" -X POST -d '{"c": [[2.37113,48.88503],[2.37941,48.88837],[2.37123,48.88379]]}' http://localhost:9900/api/cycling

# route/segments as geojson
curl "http://localhost:9900/api/cycling/route.geojson?c=2.37113&c=48.88503&c=2.37941&c=48.88837&c=2.37123&c=48.88379"
curl "http://localhost:9900/api/cycling/segments.geojson?c=2.37113&c=48.88503&c=2.37941&c=48.88837&c=2.37123&c=48.88379"
curl -G \
    --data-urlencode "c=2.37113" \
    --data-urlencode "c=48.88503" \
    --data-urlencode "c=2.37941" \
    --data-urlencode "c=48.88837" \
    --data-urlencode "c=2.37123" \
    --data-urlencode "c=48.88379" \
    -X GET http://localhost:9900/api/cycling/route.geojson
#   -X GET http://localhost:9900/api/cycling/segments.geojson

# route + segments
curl -H "Content-Type: application/json" -X POST -d '{"c": [[2.37113,48.88503],[2.37941,48.88837],[2.37123,48.88379]]}' https://api.mpr.guilhemallaman.net/api/cycling

# route/segments as geojson
curl "https://api.mpr.guilhemallaman.net/api/cycling/route.geojson?c=2.37113&c=48.88503&c=2.37941&c=48.88837&c=2.37123&c=48.88379"
curl "https://api.mpr.guilhemallaman.net/api/cycling/segments.geojson?c=2.37113&c=48.88503&c=2.37941&c=48.88837&c=2.37123&c=48.88379"
```
