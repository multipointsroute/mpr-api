#! /bin/bash

export PROJECT=mpr-api

sonar-scanner \
  -Dsonar.hostUrl="$SONAR_HOST" \
  -Dsonar.projectName="$PROJECT" \
  -Dsonar.projectKey="$PROJECT" \
  -Dsonar.projectVersion="$API_VERSION" \
  -Dsonar.login="$SONAR_LOGIN" \
  -Dsonar.sources=mpr \
  -Dsonar.tests=tests \
  -Dsonar.python.coverage.reportPaths=.coverage.xml \
  -Dsonar.qualitygate.wait=true
