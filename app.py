from logging.config import fileConfig

from mpr.app import create_app

fileConfig("logging.conf", disable_existing_loggers=True)

application = create_app()
