PREFIX = "/api"

SRID = "EPSG:4326"

ORS_API_V2_URL = "https://api.openrouteservice.org/v2/directions/{}/geojson"

ORS_MODES = {
    "driving": "driving-car",
    "walking": "foot-walking",
    "cycling": "cycling-regular",
}
