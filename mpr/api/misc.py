from typing import Dict

from flask.views import MethodView
from flask_smorest import Blueprint

import settings
from mpr.extensions import DEFAULT_CACHE_TIMEOUT, cache
from mpr.schema.misc import ApiTitleSchema, ApiVersionSchema

blp = Blueprint("Misc", __name__, description="Misc endpoints")


@blp.route("/title")
class ApiTitleView(MethodView):
    @blp.response(200, schema=ApiTitleSchema)
    @cache.cached(DEFAULT_CACHE_TIMEOUT)
    def get(self) -> Dict:
        """API title"""
        return dict(title=settings.API_TITLE)


@blp.route("/version")
class ApiVersionView(MethodView):
    @blp.response(200, schema=ApiVersionSchema)
    @cache.cached(DEFAULT_CACHE_TIMEOUT)
    def get(self) -> Dict:
        """API version"""
        return dict(version=settings.API_VERSION)
