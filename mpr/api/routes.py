import logging
from typing import Dict, List, Tuple

import requests
from flask import current_app
from flask.views import MethodView
from flask_smorest import Blueprint
from shapely.geometry import LineString, Point, shape

from mpr.api import ORS_API_V2_URL, ORS_MODES
from mpr.schema.routes import (
    FullRouteResponseSchema,
    RouteFeatureSchema,
    RouteGetRequestBody,
    RouteRequestBody,
    RouteRequestPath,
    SegmentsFeatureCollectionSchema,
)

blp = Blueprint("Routes", __name__, description="Compute routes between possible multiple points")


def fetch_ors_route(ors_mode: str, coordinates: List[List[float]]) -> Dict:
    headers = {
        "Accept": "application/json, text/plain, */*",
        "Accept-Encoding": "gzip, deflate, br",
        "Authorization": current_app.config["ORS_API_KEY"],
        "Content-Type": "application/json",
    }
    url = ORS_API_V2_URL.format(ors_mode)
    req = requests.post(url, headers=headers, json={"coordinates": [[p[0], p[1]] for p in coordinates]})
    return req.json()


def process_ors_route(provider: str, mode: str, coordinates: List[List[float]]) -> Tuple[Dict, List]:

    json = fetch_ors_route(ORS_MODES[mode], coordinates)
    feature = json["features"][0]
    summary = feature["properties"]["summary"]
    summary["provider"] = provider
    summary["mode"] = mode

    waypoints = [Point(p[0], p[1]) for p in feature["geometry"]["coordinates"]]
    logging.getLogger(__name__).info(f"Route requested through {len(coordinates)} coordinates uses {len(waypoints)} waypoints")

    segments = []
    for segment in feature["properties"]["segments"]:
        for step in segment["steps"]:
            segments.append({"geometry": LineString([waypoints[i] for i in step["way_points"]]), "properties": step})

    return {"bbox": feature["bbox"], "geometry": shape(feature["geometry"]), "properties": summary}, segments


@blp.route("/<string:mode>")
class RoutesView(MethodView):
    @blp.arguments(RouteRequestPath, location="path", as_kwargs=True)
    @blp.arguments(RouteRequestBody, location="json", as_kwargs=True)
    @blp.response(200, schema=FullRouteResponseSchema)
    def post(self, **kwargs) -> Dict:
        """Compute route and segments through multiple points"""
        route, segments = process_ors_route(kwargs.pop("provider", "ors"), kwargs.pop("mode"), kwargs.pop("c"))
        return {"route": route, "segments": {"features": segments}}


@blp.route("/<string:mode>/route.geojson")
class GeojsonRouteView(MethodView):
    @blp.arguments(RouteRequestPath, location="path", as_kwargs=True)
    @blp.arguments(RouteGetRequestBody, location="query", as_kwargs=True)
    @blp.response(200, schema=RouteFeatureSchema)
    def get(self, **kwargs) -> Dict:
        """Compute geojson route through multiple points"""
        raw = kwargs.pop("c")
        coordinates = [[raw[i], raw[i + 1]] for i, _ in enumerate(raw) if not i % 2]
        route, _ = process_ors_route(kwargs.pop("provider", "ors"), kwargs.pop("mode"), coordinates)
        return route


@blp.route("/<string:mode>/segments.geojson")
class GeojsonSegmentsView(MethodView):
    @blp.arguments(RouteRequestPath, location="path", as_kwargs=True)
    @blp.arguments(RouteGetRequestBody, location="query", as_kwargs=True)
    @blp.response(200, schema=SegmentsFeatureCollectionSchema)
    def get(self, **kwargs):
        """Compute geojson segments through multiple points"""
        raw = kwargs.pop("c")
        coordinates = [[raw[i], raw[i + 1]] for i, _ in enumerate(raw) if not i % 2]
        _, segments = process_ors_route(kwargs.pop("provider", "ors"), kwargs.pop("mode"), coordinates)
        return {"features": segments}
