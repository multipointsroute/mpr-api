import logging
import time

import httpagentparser
from flask import Flask, Response, g, jsonify, make_response, request
from flask_smorest import Api
from werkzeug.exceptions import HTTPException

import settings
from mpr.api import PREFIX
from mpr.api.misc import blp as blp_misc
from mpr.api.routes import blp as blp_routes
from mpr.extensions import cache, cors
from mpr.schema.geofields import (
    GeometryField,
    LineStringField,
    PointField,
    PolygonField,
)


def register_extensions(app: Flask) -> None:
    cache.init_app(app)
    cors.init_app(app)


def register_hooks(app: Flask) -> None:
    @app.before_request
    def before_request() -> None:
        g.start_time = time.time()

    @app.after_request
    def after_request(response: Response) -> Response:
        elapsed = time.time() - g.start_time

        # try parse user agent
        ua = request.user_agent.string
        parsed = httpagentparser.detect(ua)
        if "os" in parsed and "browser" in parsed:
            os, browser = httpagentparser.simple_detect(ua)
            ua = f"{browser} on {os}"

        msg = "[{ip}] {method} {uri} -> {code} t: {time}s (agent: {user_agent})".format(
            ip=request.headers.getlist("X-Forwarded-For")[0] if request.headers.getlist("X-Forwarded-For") else request.remote_addr,
            method=request.method,
            uri=request.path,
            code=response.status_code,
            time=round(elapsed, 3),
            user_agent=ua,
        )
        logging.getLogger(__name__).info(msg)
        return response


def register_error_handlers(app: Flask) -> None:
    @app.errorhandler(HTTPException)
    def handle_error(err: HTTPException) -> Response:
        logging.getLogger(__name__).error(err)
        payload = dict(code=err.code, status=err.name, message=err.description)
        return make_response(jsonify(payload), err.code)


def register_api(app: Flask) -> Api:
    api = Api(app)
    api.register_blueprint(blp_misc, url_prefix=PREFIX)
    api.register_blueprint(blp_routes, url_prefix=PREFIX)
    api.register_field(GeometryField, "GeoJSON", None)
    api.register_field(PointField, "GeoJSON <Point>", None)
    api.register_field(LineStringField, "GeoJSON <LineString>", None)
    api.register_field(PolygonField, "GeoJSON <Polygon>", None)
    return api


def create_app(config_object=settings) -> Flask:
    """
    WSGI App factory as per the Flask app factory pattern http://flask.pocoo.org/docs/1.0/patterns/appfactories/
    """
    app = Flask(__name__)
    app.config.from_object(config_object)
    register_extensions(app)
    register_hooks(app)
    register_error_handlers(app)
    register_api(app)
    return app
