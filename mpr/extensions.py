from flask_caching import Cache
from flask_cors import CORS

DEFAULT_CACHE_TIMEOUT = 60
cache: Cache = Cache()
cors: CORS = CORS()
