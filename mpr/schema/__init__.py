import json
import os

from jsonschema import Draft4Validator, RefResolver
from marshmallow import Schema, fields, validate

from mpr.schema.geofields import (
    GeometryField,
    LineStringField,
    PointField,
    PolygonField,
)


class GeojsonFeatureSchema(Schema):
    type = fields.String(validate=validate.Equal("Feature"), default="Feature")
    geometry = GeometryField(required=True)
    properties = fields.Nested(fields.Dict, default={})


class GeojsonFeatureCollectionSchema(Schema):
    type = fields.String(validate=validate.Equal("FeatureCollection"), default="FeatureCollection")
    features = fields.List(fields.Nested(GeojsonFeatureSchema), required=True)


def geojson_validator() -> Draft4Validator:

    schema_folder = os.path.join(os.path.abspath(os.path.dirname(__file__)), "geojson")

    geojson_base = json.load(open(os.path.join(schema_folder, "geojson.json")))

    # pre-cache the associated schema data so that we don't have to connect to the the given URLs (which don't exist)
    cached_json = {
        "https://json-schema.org/geojson/crs.json": json.load(open(os.path.join(schema_folder, "crs.json"))),
        "https://json-schema.org/geojson/bbox.json": json.load(open(os.path.join(schema_folder, "bbox.json"))),
        "https://json-schema.org/geojson/geometry.json": json.load(open(os.path.join(schema_folder, "geometry.json"))),
    }
    resolver = RefResolver("https://json-schema.org/geojson/geojson.json", geojson_base, store=cached_json)

    return Draft4Validator(geojson_base, resolver=resolver)
