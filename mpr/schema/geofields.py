from typing import Any, Dict, Mapping, Optional

from marshmallow import ValidationError, fields
from shapely.geometry import mapping, shape
from shapely.geometry.base import BaseGeometry


class GeometryField(fields.Field):
    __type__: Optional[str]

    def _serialize(self, value: Any, attr: str, obj: Any, **kwargs) -> Optional[Dict]:
        if value is None:
            return None
        try:
            return mapping(value)
        except ValueError as error:
            raise ValidationError("Invalid geometry") from error

    def _deserialize(self, value: Any, attr: Optional[str], data: Optional[Mapping[str, Any]], **kwargs) -> Optional[BaseGeometry]:
        if value is None:
            return None
        if "type" not in value or (self.__type__ is not None and value["type"] != self.__type__):
            raise ValidationError("Wrong GeoJSON Type")
        try:
            return shape(value)
        except ValueError as error:
            raise ValidationError("Invalid GeoJSON") from error


class PointField(GeometryField):
    __type__ = "Point"


class LineStringField(GeometryField):
    __type__ = "LineString"


class PolygonField(GeometryField):
    __type__ = "Polygon"
