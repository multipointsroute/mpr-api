from marshmallow import Schema, fields


class ApiTitleSchema(Schema):
    title = fields.String(metadata=dict(description="Api title"))


class ApiVersionSchema(Schema):
    version = fields.String(metadata=dict(description="Api version"))
