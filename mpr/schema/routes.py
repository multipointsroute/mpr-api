from marshmallow import Schema, fields, validate

from mpr.api import ORS_MODES, SRID
from mpr.schema import GeojsonFeatureCollectionSchema, GeojsonFeatureSchema
from mpr.schema.geofields import LineStringField


class RouteRequestPath(Schema):
    mode = fields.String(validate=validate.OneOf(ORS_MODES.keys()), metadata=dict(description="Moving mode"))


class RouteRequestBody(Schema):
    provider = fields.String(validate=validate.OneOf(["ors"]), default="ors", metadata=dict(description="Route computing provider"))
    c = fields.List(
        fields.List(fields.Float, validate=validate.Length(2)),
        required=True,
        validate=validate.Length(min=2),
        metadata=dict(description="Ordered list of coordinates to go through, list like [lon, lat]"),
    )


class RouteGetRequestBody(RouteRequestBody):
    c = fields.List(
        fields.Number,
        required=True,
        validate=validate.Length(min=4),
        metadata=dict(description="List of coordinates to go through, ordered like lon1,lat1,lon2,lat2"),
    )


class RoutePropertiesSchema(Schema):
    provider = fields.String(validate=validate.OneOf(["ors"]), metadata=dict(description="Route computing provider"))
    mode = fields.String(validate=validate.OneOf(ORS_MODES.keys()), metadata=dict(description="Moving mode"))
    distance = fields.Float(validate=validate.Range(min=0, min_inclusive=False), metadata=dict(description="Total route distance"))
    duration = fields.Float(validate=validate.Range(min=0, min_inclusive=False), metadata=dict(description="Total route duration"))


class RouteFeatureSchema(GeojsonFeatureSchema):
    bbox = fields.List(
        fields.Number,
        validate=validate.Length(4),
        metadata=dict(
            description=f"Bounding box route (using projection: {SRID})",
        ),
    )
    geometry = LineStringField(required=True, metadata=dict(description=f"Geojson line geometry for the route (using projection {SRID})"))
    properties = fields.Nested(RoutePropertiesSchema)


class SegmentPropertiesSchema(Schema):
    name = fields.String(metadata=dict(description="Segment name"))
    instruction = fields.String(metadata=dict(description="Segment instruction"))
    distance = fields.Float(validate=validate.Range(min=0, min_inclusive=False), metadata=dict(description="Segment distance"))
    duration = fields.Float(validate=validate.Range(min=0, min_inclusive=False), metadata=dict(description="Segment duration"))


class SegmentFeatureSchema(GeojsonFeatureSchema):
    geometry = LineStringField(required=True, metadata=dict(description=f"GeoJSON line geometry for the segment (using projection {SRID})"))
    properties = fields.Nested(SegmentPropertiesSchema)


class SegmentsFeatureCollectionSchema(GeojsonFeatureCollectionSchema):
    features = fields.List(fields.Nested(SegmentFeatureSchema))


class FullRouteResponseSchema(Schema):
    route = fields.Nested(RouteFeatureSchema, metadata=dict(description="Computed route"))
    segments = fields.Nested(SegmentsFeatureCollectionSchema, metadata=dict(description="Route's segments"))
