import json
import logging
from typing import Any


def pretty_format(data: Any) -> str:
    return json.dumps(data, sort_keys=True, indent=4, default=lambda d: str(d))


def pretty_log(data: Any, level=logging.DEBUG) -> None:
    logging.getLogger(__name__).log(level, pretty_format(data))


def pretty_print(data: Any) -> None:
    print(pretty_format(data))
