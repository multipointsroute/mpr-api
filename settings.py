import os

# Flask
FLASK_ENV = os.environ.get("FLASK_ENV", "production")
FLASK_DEBUG = os.environ.get("FLASK_DEBUG", False)
FLASK_USE_RELOAD = FLASK_DEBUG
SHOW_STACKTRACE = FLASK_DEBUG

# ORS
ORS_API_KEY = os.environ.get("ORS_API_KEY")

# API
API_TITLE = "MultiPointsRoute API"
API_VERSION = os.environ.get("API_VERSION")
JSON_AS_ASCII = False
ERROR_404_HELP = False
ENABLE_PROXY_FIX = True  # Extract and use X-Forwarded-For/X-Forwarded-Proto headers?
PAGINATE_DEFAULT_LIMIT = 50

# OPENAPI
OPENAPI_VERSION = "3.0.3"
OPENAPI_URL_PREFIX = "/openapi"
# SWAGGER UI
OPENAPI_SWAGGER_UI_PATH = "/swagger"
OPENAPI_SWAGGER_UI_URL = "https://cdn.jsdelivr.net/npm/swagger-ui-dist/"

# CACHING
CACHE_TYPE = "RedisCache"
CACHE_DEFAULT_TIMEOUT = 500
CACHE_REDIS_HOST = os.environ.get("CACHE_REDIS_HOST", "localhost")
CACHE_REDIS_PORT = os.environ.get("CACHE_REDIS_PORT", 6379)
CACHE_REDIS_DB = 0
CACHE_KEY_PREFIX = "mpr."
