from typing import Generator

import pytest
from flask.testing import FlaskClient

import settings
from mpr.app import create_app


@pytest.fixture(scope="session")
def app() -> Generator:
    setattr(settings, "FLASK_ENV", "test")
    setattr(settings, "FLASK_DEBUG", True)
    setattr(settings, "API_TITLE", "Test API")
    setattr(settings, "API_VERSION", "vtest")
    setattr(settings, "TESTING", True)
    app = create_app(settings)
    yield app


@pytest.fixture()
def context(app):
    with app.test_request_context():
        yield app


@pytest.fixture()
def client(app) -> FlaskClient:
    return app.test_client()


def pytest_configure(config):
    # executed only once before tests start
    pass


@pytest.fixture(autouse=True)
def run_around_tests(app):
    # executed around every unit test
    pass
