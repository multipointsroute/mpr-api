from flask.testing import FlaskClient


def test_get_title(client: FlaskClient):
    r = client.get("/api/title")
    assert r.status_code == 200
    assert r.get_json()["title"] == "Test API"


def test_get_version(client: FlaskClient):
    r = client.get("/api/version")
    assert r.status_code == 200
    assert r.get_json()["version"] == "vtest"
