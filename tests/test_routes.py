import json
import os

from flask.testing import FlaskClient
from jsonschema import ValidationError
from pytest import fail
from requests_mock import Mocker

from mpr.api import ORS_API_V2_URL, ORS_MODES
from mpr.schema import geojson_validator

coords = [[2.37113, 48.88503], [2.37941, 48.88837], [2.37123, 48.88379]]
coords_query = "c=2.37113&c=48.88503&c=2.37941&c=48.88837&c=2.37123&c=48.88379"


def patch_requests_mock(ors_mode: str, requests_mock: Mocker):
    with open(os.path.join("tests", "mocks", f"ors_v2_directions_{ors_mode}.geojson")) as ors_geojson:
        requests_mock.post(ORS_API_V2_URL.format(ors_mode), json=json.load(ors_geojson))


def test_routes_ok(client: FlaskClient, requests_mock: Mocker):

    for mode in ORS_MODES.keys():
        patch_requests_mock(ORS_MODES[mode], requests_mock)

        # full route (route + segments) with POST
        assert client.post(f"/api/{mode}", json={"c": coords}, content_type="application/json").status_code == 200

        # geojson route with GET
        assert client.get(f"/api/{mode}/route.geojson?{coords_query}").status_code == 200
        # geojson segments with GET
        assert client.get(f"/api/{mode}/segments.geojson?{coords_query}").status_code == 200


def test_valid_geojsons(client: FlaskClient, requests_mock: Mocker):
    validator = geojson_validator()
    for mode in ORS_MODES.keys():
        patch_requests_mock(ORS_MODES[mode], requests_mock)

        full = client.post(f"/api/{mode}", json={"c": coords}, content_type="application/json").get_json()
        route = client.get(f"/api/{mode}/route.geojson?{coords_query}").get_json()
        segments = client.get(f"/api/{mode}/route.geojson?{coords_query}").get_json()

        try:
            validator.validate(full["route"])
            validator.validate(full["segments"])
            validator.validate(route)
            validator.validate(segments)
        except ValidationError:
            fail("Invalid geojson")
